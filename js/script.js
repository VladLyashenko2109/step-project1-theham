window.addEventListener('scroll', function() {
    const parallaxContainer = document.querySelector('.background-header');
    let scrollPosition = window.pageYOffset;
 
    parallaxContainer.style.backgroundPositionY = scrollPosition * 0.01 + 'px';
});

const ourServices = document.querySelectorAll('.find-nav');
const pagraphesOfOurServices = document.querySelectorAll('.find-p');


ourServices.forEach((menu) => {
    menu.addEventListener('mousedown', (e) => {
                e.preventDefault();
            ourServices.forEach((nav) => {
                nav.classList.remove('on-click');
            })
            pagraphesOfOurServices.forEach((p) => {
                p.classList.remove('active-p');
                if(menu.dataset.nav === p.dataset.p) {
                    p.classList.add('active-p');
                   
                }
                
            });
            menu.classList.add('on-click');
        
        });
});
   

const buttonLoadMore = document.querySelector('.load-more-in-gallery');

const moreImg = document.querySelector('.more-img');
const moreImg2 = document.querySelector('.more-img2');
const loader = document.querySelector('.loader');


let interval1;
let intervalImg1;
let timeOut1;

let interval2;
let intervalImg2;
let timeOut2;


buttonLoadMore.addEventListener('mousedown' , (e) => {
    
   if(e.target.className === 'load-more-in-gallery' || e.target.classList.toggle('first-click-load-more')){
    
    interval1 = setInterval(() => { 
        loader.style.display = 'block'
    }, 1000);

    intervalImg1 = setInterval(() => { 
        moreImg.style.display = 'block'
    }, 5000);

    timeOut1 = setTimeout(() => {
        clearInterval(interval1);
        loader.style.display = 'none';
        e.target.classList.toggle('first-click-load-more')
        
    }, 5000);    
    
} else if(e.target.className === 'load-more-in-gallery' || e.target.classList.toggle('second-click-load-more')) {
        
        interval2 = setInterval(() => { 
            loader.style.display = 'block'
        }, 1000);
    
        intervalImg2 = setInterval(() => { 
            moreImg2.style.display = 'block'
        }, 2000);
    
        timeOut2 = setTimeout(() => {
            clearInterval(interval2);
            loader.style.display = 'none';
            buttonLoadMore.style.display = 'none';
        }, 3000);    
    }
  
});


const navOurAmazingWork = document.querySelector('.nav-our-amazing-work').children;
const galleryOfOuAmazingWork = document.querySelector('.gallery-of-our-amazing-work').children;


[...navOurAmazingWork].forEach((nav) => {
    nav.addEventListener('mouseup' , (e) => {
        e.preventDefault();
        
        [...galleryOfOuAmazingWork].forEach((photo) => {
            if(nav.dataset.nav === 'all') {
                photo.style.cssText = `display: block;`
            } else if (nav.dataset.nav !== photo.dataset.gallery) {
                photo.style.cssText = `display: none;` 
            }
            if(nav.dataset.nav === photo.dataset.gallery) {
                photo.style.cssText = `display: block;`
            }
        
         
        });  
        
    });
});


const blocksOfPeople = document.getElementsByClassName('block-of-people-item');
const blockChangePerson = document.getElementsByClassName('block-change-person-item');   

const changeLeft = document.getElementById('change-left');
const changeRight = document.getElementById('change-right');



let activeBlock = document.querySelector('.activeblock');



let numberBlock = 1;

let changePersonInfo = function(numberBlock) {
    let num;
    if(numberBlock > blocksOfPeople.length) {
        numberBlock = 1;
    } 
    if(numberBlock < 1) {
        numberBlock = blocksOfPeople.length;
    }

for(num = 0; num < blocksOfPeople.length; num++) {
    blocksOfPeople[num].classList.remove('activeblock');
}

for(num = 0; num < blockChangePerson.length; num++) {
    blockChangePerson[num].classList.remove('margin-top-active');
}

blocksOfPeople[numberBlock - 1].classList.add('activeblock');
blockChangePerson[numberBlock - 1].classList.add('margin-top-active');
    
}

changePersonInfo(numberBlock);

changeLeft.addEventListener ('mousedown' , (e) => {
    changePersonInfo(numberBlock -= 1);
})

changeRight.addEventListener ('mousedown' , (e) => {
    changePersonInfo(numberBlock += 1);
})




const forClickOnItem = document.querySelector('.block-change-person').children; 
const blockActiveOnClick = document.querySelector('.block-of-people').children;




[...forClickOnItem].forEach((item) => {
    item.addEventListener('mousedown' , (e) => {
        [...blockActiveOnClick].forEach((block) => {
            block.classList.remove('activeblock');
            item.classList.remove('margin-top-active');
            if(e.target.dataset.item === block.dataset.block) {
                block.classList.add('activeblock');
                item.classList.add('margin-top-active');
            }
        });
});

});